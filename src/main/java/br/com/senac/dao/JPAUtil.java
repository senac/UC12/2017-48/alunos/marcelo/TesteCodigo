package br.com.senac.dao;


import br.com.senac.modelo.Carro;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
    
    private static EntityManagerFactory emf
            = Persistence.createEntityManagerFactory("TestePU");
    
    public static EntityManager getEntityManager() {
        try {
            return emf.createEntityManager();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Erro ao acessar banco!!!");
        }
    }
    
    public static void main(String[] args) {
        
        EntityManager em = getEntityManager();
        
        Carro carro = new Carro();
        
        carro.setAno("2005");
        carro.setCor("Branca");
        carro.setModelo("HB-20");
        carro.setPlaca("MPA-3333");
        carro.setFabricante("Hyundai");
        
        em.getTransaction().begin();
        em.persist(carro);
        em.getTransaction().commit();
        em.close();
        
    }
    
}
